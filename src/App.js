import React, {Component} from 'react';
import Cards from "./components/cards"

class App extends Component {
    render() {
        return (
            <div>
                <Cards />
            </div>
        );
    }
}

export default App;